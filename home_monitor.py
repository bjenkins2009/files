"""
Home monitoring solution

2018/2/5

DEFAULT 1-wire pin is BCM 4 (pin7)
"""
#import sys
#import RPi.GPIO as GPIO
import os
from time import sleep
import time
#import Adafruit__DHT
import glob

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '10*')[0]
device_file = device_folder + '/w1_slave'

DEBUG_flag = 1
DHTpin = 23

def read_raw_temp():
    if DEBUG_flag:
        print 'read_temp_raw start'
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    if DEBUG_flag:
        print 'read_temp start'
    lines = read_temp_raw()
    loops = 0
    while lines[0].strip()[-3:] != 'YES':
        loops = loops + 1
        if DEBUG_flag:
            print 'read_temp while loop #' + str(loops)
        sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0/5.0 + 32.0
        return temp_c, temp_f, temp_string

def getSensorData():
    if DEBUG_flag:
        print 'getSensorData start'

    temp_c, temp_f, temp_str = read_temp()

def main():
    print 'starting...'

    while True:
        try:
            _, _, temp_str = read_temp()

            print temp_str

            sleep(20)
        except:
            print 'trying again in 1 seconds..'
            sleep(1)

if __name__ == '__main__':
    main()