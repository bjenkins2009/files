"""
My First Internet of Things

Temperature.Humidity monitor using Raspberry Pi, DHT11
Data is displayed at thingspeak.com
2017/12/14
SolderingSunday.com

Based on project by Mahesh Venkitachalam at electronut.in

"""
# Import all the libraries we need to run
import sys
import RPi.GPIO as GPIO
import os
from time import sleep
import time
#import Adafruit_DHT
import urllib2
import glob
#from beebotte import *

#_accesskey = 'd04d38e71260c1ec0df8c5db69c0e995'
#_secretkey = '24edd12991917c0bbe6b8aa74a69119115851f0244a4c2410bb9fe6bc02c3c48'
#_hostname = 'api.beebotte.com'
# bclient = BBT( _accesskey, _secretkey, hostname = _hostname)
#bclient = BBT(_accesskey, _secretkey)

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '10*')[0]
device_file = device_folder + '/w1_slave'

DEBUG = 1
# Setup the pins we are connected to
# RCpin = 24
DHTpin = 23

# Set temperature and switch command
disableheater = 1
setpoint1 = 18
hystTime = 120
urlON = 'http://192.168.1.125:3000/plugs/8006C2328FB6C33F2EE1A139742645B6183B5BDF/on'
# urlON = 'http://127.0.0.1.125:3000/plugs/8006C2328FB6C33F2EE1A139742645B6183B5BDF/on'
urlOFF = 'http://192.168.1.125:3000/plugs/8006C2328FB6C33F2EE1A139742645B6183B5BDF/off'
# urlOFF = 'http://127.0.0.1:3000/plugs/8006C2328FB6C33F2EE1A139742645B6183B5BDF/off'

# Setup our API and delay
myAPI = 'U5DAT421TFYHXAEF'
myDelay = 15  # how many seconds between posting data


# GPIO.setmode(GPIO.BCM)
# GPIO.setup(RCpin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def read_temp_raw():
    print
    'read_temp_raw start'
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    print
    'read_temp_raw stop'
    return lines


def read_temp():
    print
    'read_temp start'
    lines = read_temp_raw()
    loops = 0
    while lines[0].strip()[-3:] != 'YES':
        loops = loops + 1
        print
        'read_temp while loop:' + str(loops)
        sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos + 2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        print
        'read_temp stop'
        return temp_c, temp_f


def getSensorData():
    print
    'getSensorData start'
 #   RHW, TW = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, DHTpin)

    # Convert from Celcius to Farenheit
 #   TWF = 9.0 / 5.0 * TW + 32.0

    temp_c, temp_f = read_temp()
    RHW = 0
    TW = 0
    TWF = 0

    print
    'getSensorData stop'
    # return dict
    return (str(RHW), str(TW), str(TWF), str(temp_c), str(temp_f))


def toggleSwitch(onoff, oldstatus):
    if disableheater:
        print
        'heater is diabled'
        return -1

    switchState = -1
    if onoff != int(oldstatus):
        if onoff == 1:
            urlUse = urlON
        else:
            urlUse = urlOFF
        if onoff == -1:  # then this is an initialization call
            onoff = 0

        loopcount = 0
        while onoff != switchState:
            loopcount = loopcount + 1
            print
            'toggleSwitch loop number:' + str(loopcount)
            switchResponse = urllib2.urlopen(urlUse)
            print
            'response obtained'
            switchResponseHTML = switchResponse.read()
            print
            'html removed'
            # print switchResponse
            # print switchResponse.info()
            # print switchResponseHTML
            switchResponse.close()
            equals_pos = switchResponseHTML.find('relay_state')
            if equals_pos != -1:
                print
                'relay state reported from device: ' + switchResponseHTML[equals_pos + 13:equals_pos + 14]
                switchState = int(float(switchResponseHTML[equals_pos + 13:equals_pos + 14]))

        if onoff == 1:
            switchStatus = True
        else:
            switchStatus = False
        return switchStatus
    else:
        return oldstatus

        # switchStatus = False
        # switchResponse.close()


# main() function
def main():
    print
    'starting...'

    switchStatus = toggleSwitch(-1, False)

    timeOFF = time.time() - hystTime

    baseURL = 'https://api.thingspeak.com/update?api_key=%s' % myAPI
    print
    baseURL

    while True:
        try:
            RHW, TW, TWF, temp_cstr, temp_fstr = getSensorData()

            if disableheater:
                print
                'mode: record data, heater disabled'
            else:
                print
                'mode: maintain temperature'
            # LT = RCtime(RCpin)
 #           f = urllib2.urlopen(baseURL +
 #                               "&field1=%s&field2=%s&field3=%s&field4=%s&field5=%s" % (
 #                               TW, TWF, RHW, temp_cstr, temp_fstr))
 #           print
 #           f.read()
            print
            TW + " " + TWF + " " + RHW + " " + temp_cstr + " " + temp_fstr
 #           f.close()

            #            bclient.writeBulk("rpi002", [
            #                {"resource": "T1_f", "data": temp_fstr},
            #                {"resource": "T1_c", "data": temp_cstr}
            #            ])
            #            bclient.write('rpi002', 'T2_f', '75')

            # temp_c, temp_f = read_temp()
            temp_c = float(temp_cstr)

            if temp_c >= setpoint1:
                print
                'above setpoint: ' + str(setpoint1) + ', current status: ' + str(switchStatus)
                if switchStatus:
                    print
                    'turning off switch'
                    switchStatus = toggleSwitch(0, switchStatus)
                    # switchResponse = urllib2.urlopen(urlOFF)
                    # switchStatus = False
                    timeOFF = time.time()
                    # switchResponse.close()

            else:
                print
                'below setpoint: ' + str(setpoint1) + ', current status: ' + str(switchStatus)
                if (time.time() - timeOFF) >= hystTime and not switchStatus:
                    print
                    'turning on switch'
                    # switchResponse = urllib2.urlopen(urlON)
                    # switchStatus = True
                    # switchResponse.close()
                    switchStatus = toggleSwitch(1, switchStatus)

            sleep(int(myDelay))
        except:
            print
            'trying again in 10 seconds...'
            sleep(10)


# call main
if __name__ == '__main__':
    main()
